import socket
from datetime import datetime

curr_date = datetime.now().strftime("%d-%m-%Y %H:%M")
current_datetime = bytearray(str(curr_date), encoding='utf-8')

sock = socket.socket()
sock.bind(('', 1303))
sock.listen(1)

while True:
    conn, addr = sock.accept()
    print('connected:', addr[0])
    if addr[0]:
        conn.send(current_datetime)
        conn.close()
